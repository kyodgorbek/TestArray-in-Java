# TestArray-in-Java


import java.util.Arrays;

   public class TestArrays {
    
    public static void main(String[] args){
    
     int[] ints = new int[10];
     double doubs[] = {3.4, 55, 20, 65, 7};
     String months[] = {"January", "February", "March", "Aprel", "May", "June"};
     
    // Fill array ints with a single random value
    Arrays.fill(ints, (int) (10 * Math.random()));
    System.out.println("Contents of array ints:");
    for (int i = 0; i < ints.length; i++)
      System.out.print(ints[i] + "  ");
    System.out.println();
    
    // Sort three arrays
    Arrays.sort(ints);
    Arrays.sort(months);
    Arrays.sort(doubs);
    
    // Display array months after sort
    System.out.println("First six months in alphabetical order:");
    for (int i = 0; i < months.length; i++)
     System.out.print(months[i] + "  ");
    System.out.println();
    
    // Display  result of searching for 7 in array doubs.
    System.out.println("Number 7 is at position" + Arrays.binarySearch(doubs, 7));
    
    // Display result of comparing two equal arrays.
    int moreInts[] = ints;
    System.out.println("The value should be true - it is " + Arrays.equals(ints, moreInts));
   }
  }      
